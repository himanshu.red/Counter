public class Counter {
    private int value;

    public Counter() {
        this.value = 0;
    }

    public void incr() {
        this.value += 1;
    }

    public void decr() {
        this.value -= 1;
    }

    public void incrby(int n) {
        this.value += n;
    }

    public void decrby(int n) {
        this.value -= n;
    }

    public int getValue() {
        return this.value;
    }
}
