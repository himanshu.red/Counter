public class Main {
    public static void main(String[] args) {
        Counter counter = new Counter();

        counter.incr();
        counter.incr();

        System.out.println("Current value : " + counter.getValue());

        counter.incrby(5);

        System.out.println("Current value : " + counter.getValue());

        counter.decrby(2);
        counter.decr();

        System.out.println("Current value : " + counter.getValue());

    }
}
